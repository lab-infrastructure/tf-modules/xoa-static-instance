# Set these variables in environment or terraform.tfvars

variable "realm" {
    description = "Realm - Domain (Default dylanlab.xyz)"
    type = string
    default = "dylanlab.xyz"
}

variable "vm_names" {
    description = "List of hostnames"
    type = list(string)
}

variable "vm_name_prefix" {
    description = "VM hostname prefix (Default '')"
    type = string 
    default = ""
}

variable "vm_name_suffix" {
    description = "VM hostname suffix (Default '')"
    type = string 
    default = ""
}

variable "vm_ipv4_addresses" {
    description = "List of IPv4 Addresses for DNS Servers"
    type = list(string)
}

variable "vm_ipv4_addresses_cidr" {
    description = "CIDR of ipv4_addresses Above - No Slash"
    type = number
    default = 24
}

variable "vm_wait_for_ip" {
    description = "Wait for XOA IP"
    type = bool
    default = false
}

variable "vm_tags" {
    description = "Additional tags for VM"
    type = list(string)
    default = []
}

variable "default_gateway" {
    description = "Default Gateway IPv4"
    type = string
}

variable "update_dns" {
    description = "Update a DNS server?"
    type = bool
    default = true
}

variable "dns_server_primary" {
    description = "DNS Server Primary"
    type = string
}

variable "dns_server_secondary" {
    description = "DNS Server Secondary"
    type = string
}

variable "dns_server_primary_key_name" {
    description = "DNS Server Key Name (For Updates)"
    type = string
    default = ""
}

variable "dns_server_primary_key_algorithm" {
    description = "DNS Server Key Algo (For Updates)"
    type = string
    default = ""
}

variable "dns_server_primary_key_secret" {
    description = "DNS Server Key Secret (For Updates)"
    type = string
    default = ""
}

variable "vm_name_description" {
    description = "VM Description"
    type = string
    default = "VM created with Terraform"
}

variable "xen_pool_name" {
    description = "XenOrchestra Pool"
    type = string
}

variable "xen_template_name" {
    description = "XenOrchestra Template"
    type = string
}

variable "xen_storage_name" {
    description = "XenOrchestra Storage"
    type = string
}

variable "xen_network_name" {
    description = "XenOrchestra Network"
    type = string
}

variable "vm_disk_size_gb" {
    description = "Disk size in Gb (Default 30)"
    default = 30
    type    = number
}

variable "vm_memory_size_gb" {
    description = "Memory in Gb (Default 4)"
    default = 4
    type    = number
}

variable "vm_cpu_count" {
    description = "CPU Count (Default 2)"
    default = 2
    type    = number
}

variable "username_ansible" {
    description = "Ansible account username"
    type = string
    default = "ansible"
}

variable "public_key_ansible" {
    type = string
    description = "Ansible account authorized key"
    default = ""
}

variable "username_admin" {
    description = "Administrator account username"
    type = string
    default = "admin"
}

variable "public_key_admin" {
    type = string
    description = "Administrator account authorized key"
    default = ""
}